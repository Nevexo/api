const chalk = require("chalk")
const config = require("../configs/general.json")
const fs = require("fs")
const path = require("path")

exports.log = (type, msg, specific) => {
    let head;
    switch(type) {
        case "verbose":
            head = chalk.grey("[VERBOSE]")
            break;
        case "highlight":
            head = chalk.magenta("[HIGHLIGHT]")
            break;
        case "error":
            head = chalk.red("[ERROR]")
            break;
        case "ok":
            head = chalk.green("[OK]")
            break;
        case "info":
            head = chalk.blue("[OK]")
            break;
        default:
            head = "[" + type.toUpperCase() + "]"
            break;
    }
    head = chalk.grey("[VM-Bot] ") + head + " " + msg
    let fileLocations = { //Defined for Windows/Linux (prolly wont work on macos but who give a toss.)
        "everything": __dirname + "/../logs/everything.log",
        "actions": __dirname + "/../logs/actions.log",
        "discord": __dirname + "/../logs/discord.log"
    }
    //if (fs.exists())
    fs.appendFile(fileLocations["everything"], "[SMYDGRID] [" + new Date() + "] [" + type.toUpperCase() + "] " + msg + "\n", (err) => {if (err) { throw err;}})
    console.log(head)
}