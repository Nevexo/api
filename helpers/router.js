const Express = require("express")
const bodyparser = require("body-parser")
const routes = require("./../configs/routes.json")
const cors = require("cors")

let service = Express()
let modules, helpers

service.use(cors())

exports.configure = (m, h) => {modules = m, helpers = h}

service.use(bodyparser.json())

service.use((req, res, next) => {
    res.header("Content-Type", "application/json")
    res.header("X-Powered-By", "SMYD Grid Supply")

    // All traffic passes here.

    next()
})

function test() {
    console.log("test")
}

exports.setup = () => {
    // Configure routes
    routes.forEach(route => {
        eval("service." + route.method + "('" + route.route + "', modules['" + route.module + "']['" + route.function + "'])")
    })
    
    setTimeout(() => {
        helpers.log("state", "[ROUTER] Initalising...")
        service.listen(3000, () => {
            helpers.log("ok", "[ROUTER] Server started.")
        })
    }, 500)
}

