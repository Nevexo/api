console.log("Initalising...")
let mode = "production"
const helpers = {
    "logModule": require("./helpers/log.js"),
    "load": require("./modules/modman/load.js"),
    "module_list": [],
    "router": require("./helpers/router.js")
}
helpers.log = helpers.logModule.log
const configlocation = "configs/", fs = require("fs")
let cfg = {}
let modules = {}

let supported_platform = true
if (process.platform != "linux") {
    supported_platform = false
    helpers.log("warning", "Started on the " + process.platform + " platform, some features may be disabled.")
}

//Code n shit
function loadConfigs(callback) {
    cfg = {} //Initalise "cfg" object which will contain all files from configs/
    fs.readdir(configlocation, function(error, files) {
        loops = 0
        files.forEach(function(file) {
            //log("BOOTSTRAP", "Loading config file: " + file)
            fs.readFile(configlocation + file, function(error, content) {
                if (error) {
                    throw error
                    exit(1)
                }else {
                    cfg[file.split(".")[0]] = JSON.parse(content)
                    helpers.log("bootstrap", "Loaded config: " + file.split(".")[0])
                    loops++
                    if (loops == files.length) {
                        callback()
                    }
                }
            })
        })
    })
}


helpers.log("verbose", "Loading configs...")
loadConfigs(() => {
    helpers.log("ok", "Configs ready.")
    console.log("------------------------\napi.nevexo.space Appliance Server\nCode Name: " + cfg.general.info.name)
    // Start modules //
    helpers.load.go(helpers, cfg, modules, () => {
        helpers.router.configure(modules, helpers) //Configure routing broker
        helpers.log("ok", "[BOOTSTRAP] Bootstrap finished, assuming start OK.")
        helpers.router.setup() //Deploy routes
    })
})