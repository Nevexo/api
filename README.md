# api.nevexo.space

The nevexo.space API is a collection of random things about me, for example:
 
 - My social pages (i.e api.nevexo.space/social/twitter)

 - My version tracker (i.e api.nevexo.space/versions/api)

 - My link shortner (i.e api.nevexo.space/s/Gusy£) -- Yes, I'll buy a short domain for this... one day.
 
 - Any other random crap i think of. 

It's entirely modular & configurable. Feel free to run your own version of it.

## Installation

- Coming soon.