var helpers;
var configs;
exports.loadModules = (callback) => {
    if (configs["modules"] == undefined) {
        helpers.log("error", "[LOADMODULES] No module version manifest.")
        process.exit(1)
    }else {
        for (var key in configs["modules"]) {
            if (configs["modules"].hasOwnProperty(key)) {
              helpers.log("state", "[BOOT] Bootstrapping " + key)
              if (configs.modules[key].enabled) {
                modules[key] = require("../../modules/" + key + "/index.js")
                helpers.module_list.push(key)
                //Now the module is live, runs it's init script:
                exports.initalise(modules[key])
              }else {
                  helpers.log("warning", "Skipping " + key + " as it's disabled in the manifest.")
              }

            }
        }
        callback()
    }
}

exports.initalise = (module) => {
    var args = {}
    module.initArgs.forEach((req) => {
        switch(req) {
            case "helpers":
                args["helpers"] = helpers
                break;
            case "module_list":
                args["module_list"] = helpers.module_list
                break;
            case "configs":
                args["configs"] = configs
                break;
            case "modules":
                args["modules"] = modules
                break;
            case "helpers_update": 
                args["helpers_update"] = exports.helpers_update
                break;
        }
    })
    module.init(args)
}

exports.helpers_update = (modName, helperName, helper) => {
    helpers.log("state", "Updating helpers with new helper from " + modName)
    helpers[helperName] = helper
}

exports.go = (h, c, m, callback) => {
    helpers = h
    configs = c
    modules = m
    exports.loadModules(() => {
        //Give everything 2 seconds to load:
        setTimeout(()=> {
            helpers.log("highlight", "Load timeout - checking module status.")
            for (var key in modules) {
                if (modules.hasOwnProperty(key)) {
                    if (modules[key].ready == false) {
                        helpers.log("error" , "Not all modules ready in time.")
                        process.exit(1)
                    }
                }
            }
            callback()
        }, 500)
    })
}

exports.modcount = () => {
    return Object.keys(modules).length
}