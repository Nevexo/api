// Social media tinggg

exports.ready = false
let helpers, configs
exports.initArgs = ["helpers", "configs"]

exports.init = (args) => {
    helpers = args.helpers, configs = args.configs
    exports.ready = true;
}

function lookup(platform) {
    if (configs.social[platform] == undefined) {
        return 404
    }else {
        return configs.social[platform]
    }
}

exports.route = (req, res) => {
    if (req.params.platform != "all") {
        let platform = lookup(req.params.platform)
        if (platform == 404) {
            res.status(404)
            res.json({"error": "invalid_platform"})
        }else {
            if (platform.url != undefined) {
                res.redirect(307, platform.url) // 307 = Temp moved (allows it to be changed ignoring peoples cache.)
            }else {
                res.json(platform) // If no URL, just send the whole object.
            }
        }
    }else {
        res.json(configs.social)
    }
}