// short-url thingo...

const randomstring = require("randomstring")
exports.ready = false
let helpers, configs, db
exports.initArgs = ["helpers", "configs"]

exports.init = (args) => {
    helpers = args.helpers, configs = args.configs
    //Check database is up
    if (helpers.db.db_ready == false) {
        helpers.log("error", "[SHORTURL] Can't start shorturl - database unavailable...")
    }
    exports.ready = true;  
}

function connect(callback) {
    if (configs.rethink != undefined) {

    }
}

function create(object, callback) {
    let exists = false;
    helpers.db.r.table('shorturls').run(helpers.db.connection, function(err, cursor) {
        if (err) throw err;
        cursor.toArray(function(err, result) {
            if (err) throw err;
            result.forEach(url => {
                if (url["code"] == object["code"]) {
                    exists = true
                }
            })
            
            if (!exists) {
                helpers.db.r.table('shorturls').insert([
                    object
                ]).run(helpers.db.connection, (err, result) => {
                    if (err) {
                        helpers.log("error", "[SURL] " + err)
                        callback(500)
                    }else {
                        callback(201)
                        helpers.log("ok", "[SURL] New Short url created: " + JSON.stringify(result))
                    }
                })
            }else {
                callback("code_exists")
            }
        });
    });
}

function edit(code, object, callback) {
    helpers.db.r.table('shorturls').filter(helpers.db.r.row("code").eq(code)).
    update(object).run(helpers.db.connection, function(err, result) {
        if (err) {
            callback(500)
            helpers.log("error", "[SURL] " + err)
        }else {
            callback(202)
        }
    });
}

exports.create = (req, res) => {
    //POST /u/create
    //Send: {"url": "flgjsdfg", "password": "shflgjksgh"} // Code can be given - if it exists cool beans if not make one with a random code
    //Return: {"short_url": "blah", "full_url": "yea", ""}
    // Checks for the body:
    if (req.body != undefined) {
        if (req.body.url != undefined) {
            // All good! Insert data:

            //Create code:
            let code = randomstring.generate(5)

            let object = {"code": code, "url": req.body.url}
            if (req.body.code != undefined) {
                object["code"] = req.body.code
            }
            if (req.body.password != undefined) {
                object["password"] = req.body.password
            }

            create(object, (rst) => {
                if (rst == "code_exists") {
                    if (object["code"] != code) {
                        // The user tried to specify a code, but it already exists:
                        res.status(208)
                        res.json({"status": "code_exists", "description": "The code sent is already registered on this server."})
                    }else {
                        res.sendStatus(500) // this is kinda hacky, just make the interface deal with it. (sorry future me)
                    }
                }else if (rst == 500) {
                    res.sendStatus(500)
                }else if (rst == 201) {
                    res.status(201)
                    res.send({"code": object["code"], "url": object["url"]})
                }
            })
            

        }else {
            res.status(400)
            res.json({"error": "missing_url"})
        }
    }else {
        res.status(400)
        res.json({"error": "invalid_body"})
    }
}

exports.get = (req, res) => {
    helpers.db.r.table('shorturls').filter(helpers.db.r.row('code').eq(req.params.code)).
    run(helpers.db.connection, function(err, cursor) {
        if (err) {
            helpers.log("error", "[SURL] " + err)
            res.sendStatus(500)
        }else {
            cursor.toArray(function(err, result) {
                if (err) {
                    helpers.log("error", "[SURL] " + err)
                    res.sendStatus(500)
                }else {
                    if (result.length == 0) {
                        res.status(404)
                        res.json({"error": "short_url_not_found"})
                    }else {
                        if (result[0].code == req.params.code) { // Check the code matches exactly.
                            res.redirect(307, result[0].url) // Redirect the user (woo!)
                        }else {
                            res.status(404)
                            res.json({"error": "short_url_not_found"})
                        }
                    }
                }
            });
        }
    });
}

exports.info = (req, res) => {
    helpers.db.r.table('shorturls').filter(helpers.db.r.row('code').eq(req.params.code)).
    run(helpers.db.connection, function(err, cursor) {
        if (err) {
            helpers.log("error", "[SURL] " + err)
            res.sendStatus(500)
        }else {
            cursor.toArray(function(err, result) {
                if (err) {
                    helpers.log("error", "[SURL] " + err)
                    res.sendStatus(500)
                }else {
                    if (result.length == 0) {
                        res.status(404)
                        res.json({"error": "short_url_not_found"})
                    }else {
                        if (result[0].code == req.params.code) { // Check the code matches exactly.
                            let object = {"code": result[0].code, "full_url": result[0].url, "editable": false}
                            if (result[0].password != undefined) {
                                object["editable"] = true;
                            }
                            res.json(object)
                        }else {
                            res.status(404)
                            res.json({"error": "short_url_not_found"})
                        }
                    }
                }
            });
        }
    });
}

exports.update = (req, res) => {
    if (req.body.url == undefined || req.body.password == undefined) {
        res.status(400)
        res.json({"error": "Missing body content."})
    }else {
        helpers.db.r.table('shorturls').filter(helpers.db.r.row('code').eq(req.params.code)).
        run(helpers.db.connection, function(err, cursor) {
            if (err) {
                helpers.log("error", "[SURL] " + err)
                res.sendStatus(500)
            }else {
                cursor.toArray(function(err, result) {
                    if (err) {
                        helpers.log("error", "[SURL] " + err)
                        res.sendStatus(500)
                    }else {
                        if (result.length == 0) {
                            res.status(404)
                            res.json({"error": "short_url_not_found"})
                        }
                        if (result[0].code == req.params.code) { // Check the code matches exactly.
                            if (result[0].password == undefined) {
                                res.status(401)
                                res.json({"error": "url_not_editable"})
                            }else {
                                if (result[0].password != req.body.password) {
                                    res.status(401)
                                    res.json({"error": "invalid_password"})
                                }else {
                                    //Password correct. Allow editing
                                    edit(req.body.code, req.body, (result) => {
                                        //omfg callback hell
                                        if (result == 500) {
                                            res.sendStatus(500)
                                        }else {
                                            res.status(202)
                                            res.json({"status": "entry_updated"})
                                        }
                                    })
                                }
                            }
                        }else {
                            res.status(404)
                            res.json({"error": "short_url_not_found"})
                        }
                    }
                });
            }
        });
    }

}

exports.delete = (req, res) => {
    if (req.headers["x-password"] == undefined) {
        res.sendStatus(401)
    }else {
        helpers.db.r.table('shorturls').filter(helpers.db.r.row('code').eq(req.params.code)).
        run(helpers.db.connection, function(err, cursor) {
            if (err) {
                helpers.log("error", "[SURL] " + err)
                res.sendStatus(500)
            }else {
                cursor.toArray(function(err, result) {
                    if (err) {
                        helpers.log("error", "[SURL] " + err)
                        res.sendStatus(500)
                    }else {
                        result.forEach(url => {
                            if (url["code"] == req.params.code) {
                                if (url["password"] != undefined) {
                                    if (url["password"] == req.headers["x-password"]) {
                                        helpers.db.r.table('shorturls').filter(helpers.db.r.row("code").eq(req.params.code)).
                                        delete().
                                        run(helpers.db.connection, function(err, result) {
                                            if (err) {
                                                res.status(500)
                                                helpers.log("error", "[SURL] " + err)
                                            }else {
                                                if (result.deleted != 0) {
                                                    res.status(202)
                                                    res.json({"status": "code_deleted"})
                                                }else {
                                                    res.status(404)
                                                    res.json({"status": "short_url_not_found"})
                                                }
                                            }
                                        });
                                    }else {
                                        res.sendStatus(401)
                                    }
                                }else {
                                    res.status(401)
                                    res.json({"error": "url_cannot_be_deleted"})
                                }
                            }
                        })
                    }
                })
            }
        })
    }
}

exports.getAll = (req, res) => {
    if (req.headers["x-password"] == undefined) {
        res.sendStatus(401)
    }else {
        helpers.db.r.table('shorturls').filter(helpers.db.r.row('password').eq(req.headers["x-password"])).
        run(helpers.db.connection, function(err, cursor) {
            if (err) {
                helpers.log("error", "[SURL] " + err)
                res.sendStatus(500)
            }else {
                cursor.toArray(function(err, result) {
                    if (err) {
                        helpers.log("error", "[SURL] " + err)
                        res.sendStatus(500)
                    }else {
                        console.dir(result)
                        let object = []
                        result.forEach(url => {
                            object.push({
                                "code": url["code"],
                                "full_url": url["url"]
                            })
                        })
                        res.json(object)
                    }
                });
            }
        });
    }
}