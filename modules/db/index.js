// Rethink/Influx manager

const r = require("rethinkdb")

exports.ready = false
exports.db_ready = false
let helpers, configs, db
exports.initArgs = ["helpers", "configs", "helpers_update"]

exports.init = (args) => {
    helpers = args.helpers, configs = args.configs
    args.helpers_update("database", "db", exports)
    // Startup database
    exports.connect()
    exports.ready = true;
}

exports.connect = () => {
    if (configs.rethink != undefined) {
        helpers.log("state", "[DB] DB Startup is pending (attempting connection)")
        exports.db_ready = "PENDING"
        r.connect(configs.rethink.auth, function(err, conn) {
            if (err) {
                exports.db_ready = false;
                helpers.log("error", "[DB] " + err)
            }else {
                exports.db_ready = true;
                helpers.log("ok", "[DB] Connected to " + configs.rethink.auth.host + "!")
                exports.connection = conn;
            }
        })
    }
}

exports.r = r